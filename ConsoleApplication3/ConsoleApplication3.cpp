﻿#include <iostream>
#include <time.h>

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	//buf.tm_mday

	const int ArrayN(5);
	
	int Array[ArrayN][ArrayN];

	for (int i(0); i < ArrayN; i++)
	{
		for (int j(0); j < ArrayN; j++)
		{
			Array[i][j] = i + j;
		}
	};

	for (int i(0); i < ArrayN; i++)
	{
		for (int j(0); j < ArrayN; j++)
		{
			std::cout << Array[i][j] << " ";
		}

		std::cout << "\n";
	};

	std::cout << std::endl;

	int Remainder = buf.tm_mday % ArrayN;

	for (int i(0); i < ArrayN; i++)
	{
		if (i == Remainder)
		{
			std::cout << "Index is " << i << "\n";
			
			int Sum(0);

			for (int j(0); j < ArrayN; j++) 
			{
				Sum += Array[i][j];
			}
			
			std::cout << "Summary is " << Sum << std::endl;
		}
	};

}
